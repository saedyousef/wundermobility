<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    use HasFactory;

    /**
     * The table associated with the model.
     *
     * @var string
    */
    protected $table = 'transactions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
    */
    protected $fillable = [
        'profile_id',
        'response_code',
        'payment_data_id',
        'status',
    ];

    /**
     * Save new Transaction
     * 
     * @return bool
     */
    public function saveTransaction(array $data)
    {
        $this->profile_id = $data['profile_id'];
        $this->response_code = $data['response_code'];
        $this->payment_data_id = $data['payment_data_id'];
        $this->status = $data['status'];

        $saved = $this->save();

        if(!$saved)
            return false;

        return true;
    }

     /**
     * Get the profile associated with the transaction.
     * 
     */
    public function profile()
    {
       return $this->belongsTo(Profile::class);
    }
}
