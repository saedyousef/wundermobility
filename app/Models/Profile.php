<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    use HasFactory;


    /**
     * The table associated with the model.
     *
     * @var string
    */
    protected $table = 'profiles';

    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
        'first_name',
        'last_name',
        'telephone',
        'street',
        'house_number',
        'city',
        'zip_code',
        'owner',
        'iban',
    ];
}
