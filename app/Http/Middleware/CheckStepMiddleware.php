<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class CheckStepMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {

        /**
         * Check for saved stage step to redirect.
         * 
         */
        if ($request->session()->has('step'))
        {
            $route_name = $request->route()->getName();

            // Work around to handle the back action.
            if ($request->has('back'))
                $request->session()->put('step', $route_name);
            
            $curren_route = $request->session()->get('step');
            if ($route_name != $curren_route)
                return redirect()->route($curren_route);
        }
    
        return $next($request);
    }
}
