<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Transaction;

class TransactionsController extends Controller
{
    public function index(Request $request)
    {
        $transactions = Transaction::orderBy('id', 'desc')->get();
        return view('transaction.index', compact('transactions'));
    }

    public function success(Request $request)
    {
        $response = $request->session()->get('response');

        return view('transaction.success', ['response' => $response]);
    }

    public function failed(Request $request)
    {
        $profile = $request->session()->get('profile');
        $response = $request->session()->get('response');

        return view('transaction.failed', ['response' => $response, 'profile' => $profile]);
    }
}
