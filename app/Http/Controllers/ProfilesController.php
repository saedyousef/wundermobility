<?php

namespace App\Http\Controllers;

use App\Http\Requests\SaveProfileAddressRequest;
use App\Http\Requests\SaveProfilePaymentRequest;
use App\Http\Requests\SaveProfileInfoRequest;
use Illuminate\Http\Request;
use App\Models\Transaction;
use App\Models\Profile;
use App\Api\WunderPay;

class ProfilesController extends Controller
{

    public function index(Request $request)
    {
        $profiles = Profile::orderBy('id', 'desc')->get();
        return view('profile.index', compact('profiles'));
    }

    /**
     * Display the first step form.
     * 
     * @param Request object
     * @return view
     */
    public function fillInfo(Request $request)
    {

        $profile = $request->session()->get('profile');
        return view('profile.create_step_one', compact('profile'));
    }

    public function postFillInfo(SaveProfileInfoRequest $request)
    {
        $data = $request->all();
        
        if (empty($request->session()->get('profile')))
        {
            $profile = new Profile();
            $profile->fill($data);
            $request->session()->put('profile', $profile);
        }
        else
        {
            $profile = $request->session()->get('profile');
            $profile->fill($data);
            $request->session()->put('profile', $profile);
        }

        $request->session()->put('step', 'steptwo');
        return redirect()->route('steptwo');
    }


    /**
     * Display the second step form.
     * 
     * @param Request object
     * @return view
     */
    public function fillAddress(Request $request)
    {
        $request->session()->put('step', 'steptwo');

        $profile = $request->session()->get('profile');
        return view('profile.create_step_two', compact('profile'));
    }


    /**
     * The latest step of our registeration process, calling the payment api.
     * 
     * @param SaveProfileAddressRequest object
     * @return view
     */
    public function postFillAddress(SaveProfileAddressRequest $request)
    {
        $data = $request->all();
        
        $profile = $request->session()->get('profile');
        $profile->fill($data);
        $request->session()->put('profile', $profile);

        $request->session()->put('step', 'stepthree');
        return redirect()->route('stepthree');
    }

    /**
     * Display the last step form.
     * 
     * @param Request object
     * @return view
     */
    public function fillPayment(Request $request)
    {
        $request->session()->put('step', 'stepthree');
        $profile = $request->session()->get('profile');
        return view('profile.create_step_three', compact('profile'));
    }

    /**
     * The latest step of our registeration process, calling the payment api.
     * 
     * @param SaveProfilePaymentRequest object
     * @return view
     */
    public function postFillPayment(SaveProfilePaymentRequest $request)
    {
        // Fetching request into data.
        $data = $request->all();
        
        // Get the profile object from session.
        $profile = $request->session()->get('profile');

        // Filling the requested fields & save.
        $profile->fill($data);
        $saved = $profile->save();

        if(!$saved)
            return view('transaction.failed', ['message' => 'Something went wrong!']);

        // Initialize instance.
        $pay = new WunderPay($profile->id, $profile->iban, $profile->owner);
        
        // Calls the payment API.
        $response = $pay->pay();
        
        // Initialize instance.
        $transaction = new Transaction();
        
        // Assigning profile_id to the response array to save it in the transaction.
        $response['profile_id'] = $profile->id;
        
        // Saves the transaction.
        $savedTransaction = $transaction->saveTransaction($response);
        
        // Checking.
        if(!$response['success'] || !$savedTransaction)
            return redirect()->route('failed');

        // Clear session.
        $request->session()->forget('step');

        // Append response to session to use it later.
        $request->session()->put('response', $response);
        
        return redirect()->route('success');       
    }
}
