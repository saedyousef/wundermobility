<?php
namespace App\Constants;

/**
 * Class Urls
 * contains Apis Urls
 */
final class Urls {

    /**
     * Urls constructor.
     * prevent instantiation.
     */
    private function __construct() {
    }

    const PAY_URL = "https://37f32cl571.execute-api.eu-central-1.amazonaws.com/default/wunderfleet-recruiting-backend-dev-save-payment-data";
}
