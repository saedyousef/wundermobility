<?php

namespace App\Api;

use App\Constants\HttpStatus;
use App\Constants\Urls;
use Illuminate\Support\Facades\Http;

class WunderPay {

    protected $owner;
    protected $iban;
    protected $customer_id;

    // WunderPay constructer.
    public function __construct($customer_id, $iban, $owner)
    {
        $this->customer_id = $customer_id;
        $this->iban = $iban;
        $this->owner = $owner;
    }

    /**
     * Make post request to the demo payment API.
     * 
     * @return array
     */
    public function pay()
    {
        // Preparing request.
        $response = Http::post(Urls::PAY_URL, [
            'customerId' => $this->customer_id,
            'iban' => $this->iban,
            'owner' => $this->owner,
        ]);
        
        // Check for response.
        if ($response->status() != HttpStatus::OK || !isset($response->json()["paymentDataId"]))
            return ['payment_data_id' => null, 'success' => false, 'status' => HttpStatus::$codesMessages[$response->status()]];
        
        // Return the paymentDataId.
        return ['payment_data_id' => $response->json()['paymentDataId'], 'success' => true, 'status' => HttpStatus::$codesMessages[$response->status()], 'response_code' => $response->status()];

    }
}