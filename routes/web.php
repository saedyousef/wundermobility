<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProfilesController;
use App\Http\Controllers\TransactionsController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [ProfilesController::class, 'fillInfo'])->middleware('steps');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::prefix('profile')->group(function () {

    Route::get('/', [ProfilesController::class, 'index'])->middleware('auth')->name('profile.index');

    Route::get('info', [ProfilesController::class, 'fillInfo'])->middleware('steps')->name('stepone');
    Route::post('info', [ProfilesController::class, 'postFillInfo'])->name('poststepone');

    Route::get('address', [ProfilesController::class, 'fillAddress'])->middleware('steps')->name('steptwo');
    Route::post('address', [ProfilesController::class, 'postFillAddress'])->name('poststeptwo');

    Route::get('payment', [ProfilesController::class, 'fillPayment'])->middleware('steps')->name('stepthree');
    Route::post('payment', [ProfilesController::class, 'postFillPayment'])->name('poststepthree');

});

Route::prefix('transaction')->group(function () {
    Route::get('/', [TransactionsController::class, 'index'])->name('transaction.index');

    Route::get('success', [TransactionsController::class, 'success'])->name('success');
    Route::get('failed', [TransactionsController::class, 'failed'])->name('failed');
});
