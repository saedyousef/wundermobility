# Wunder Mobility



### Installing

Clone this repo into your local machine

Then open the terminal and go to the directory where your project is located.

```
cd wundermobility
```

Create a new file inside the project, with the name .env

```
touch .env
```

open .env.example file and copy all file contents inside the .env file.

Create a new file inside database folder, and name it as db.sqlite, then copy the full path of this file.


Install required dependincies
```
composer install
```

Migrate 
```
php artisan migrate
```

Since there is no user registeration, run the seeder to get one user with the following credentials: `admin@example.com` password `admin123`
```
php artisan db:seed
```

Now we are ready to go.


## Possible performance optimizations for my Code

Creating a stand alone package to handle the APIs request.
Caching form data inside local storage instead of server session.
Instead of the forms request, use AJAX request.


## Which things could be done better

Creating only one form instead of form for each step, for example: use modal.
The stage step could be done in better way when clicking on `Back` button.
Using AJAX requests to store form data inside local storage, for example save the input data using onkeyUp event and make a request for each event
This task could be done as two seprated projects, one for the backend(APIs) and the other one for the clientside.

## Desing pattern or Software Architecture
I used MVC, so the views can interact with the controller in my case.

## Database Schema
Database schema is loacted under database directory, as sqlite-schema.dump
