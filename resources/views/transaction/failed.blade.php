@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Failed') }}</div>
                <div class="card-body">
                    <div class="alert alert-danger" role="alert">
                        Something went wrong!
                    </div>
                    <div class="alert alert-secondary" role="alert">
                        Your inserted data:
                    </div>
                    
                    <table class="table table-striped table-dark">
                        <thead>
                            <tr>
                            <th scope="col">Name</th>
                            <th scope="col">Telephone</th>
                            <th scope="col">IBAN</th>
                            <th scope="col">Address</th>

                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td> {{ $profile->first_name }} {{ $profile->last_name }}</td>
                                <td> {{ $profile->telephone }}</td>
                                <td> {{ $profile->iban }}</td>
                                <td> {{ $profile->street }}  {{ $profile->house_number }}, {{ $profile->city }} {{ $profile->zip_code }} </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
