@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Transactions') }}</div>
                <div class="card-body">
                    <table class="table table-striped table-dark">
                        <thead>
                            <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Owner</th>
                            <th scope="col">Payment Data Id</th>
                            <th scope="col">Status</th>
                            <th scope="col">Response Code</th>

                            </tr>
                        </thead>
                        <tbody>
                            @foreach($transactions as $transaction)
                                <tr>
                                    <th scope="row"> {{ $transaction->id }}</th>
                                    <td> {{ $transaction->profile->owner}}</td>
                                    <td> {{ $transaction->payment_data_id }}</td>
                                    <td> {{ $transaction->status }}</td>
                                    <td> {{ $transaction->response_code }}</td>
                                </tr>
                            @endforeach 
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
