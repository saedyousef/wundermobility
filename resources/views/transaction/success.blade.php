@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Success') }}</div>
                <div class="card-body">
                    <div class="alert alert-success" role="alert">
                        Action has been done successfully!
                    </div>

                    <div class="alert alert-primary" role="alert">
                       Your  PaymentDataId : {{ $response['payment_data_id'] }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
