@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">{{ __('Profiles') }}</div>
                <div class="card-body">
                    <table class="table table-striped table-dark">
                        <thead>
                            <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Name</th>
                            <th scope="col">Telephone</th>
                            <th scope="col">IBAN</th>
                            <th scope="col">Address</th>
                            <th scope="col">Created At</th>

                            </tr>
                        </thead>
                        <tbody>
                            @foreach($profiles as $profile)
                                <tr>
                                    <th scope="row"> {{ $profile->id }}</th>
                                    <td> {{ $profile->first_name }} {{ $profile->last_name }}</td>
                                    <td> {{ $profile->telephone }}</td>
                                    <td> {{ $profile->iban }}</td>
                                    <td> {{ $profile->street }}  {{ $profile->house_number }}, {{ $profile->city }} {{ $profile->zip_code }} </td>
                                    <td> {{ $profile->created_at }}</td>
                                </tr>
                            @endforeach 
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
